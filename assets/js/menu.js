(function($) {
  let toggle = document.getElementById("menu-toggle");
  let menu = document.getElementById("menu");
  let close = document.getElementById("menu-close");

  toggle.addEventListener("click", function() {
    if (menu.classList.contains("open")) {
      menu.classList.remove("open");
    } else {
      menu.classList.add("open");
    }
  });

  close.addEventListener("click", function() {
    menu.classList.remove("open");
  });

  $(window).on("resize", function() {
    if ($(window).width() < 846) {
      $(".main-menu a").on("click", function() {
        menu.classList.remove("open");
      });
    }
  });

  $(".hover").mouseleave(function() {
    $(this).removeClass("hover");
  });

})(jQuery);
